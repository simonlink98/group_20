import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


class EnergyData:
    """
    This class is the blueprint for all objects that are created to inspect the data set
    """
    
    def __init__(self):
        """
        This method initializes the data frame attribute of the class that stores the dataframe 
        and all the data used.
        """
        
        self.df = [] # dataframe object is an empty list for a start
    
    def download(self):
        """
        This method checks if the dataset is already downloaded, and if not, downloads
        the data set from the Web Source.
        Furthermore, it filters for only years after 1970, inclusively.
        """
        
        try:
            df = pd.read_csv("downloads/data.csv", index_col = 0)
            self.df = df[df['year']>=1970]
        except:
            url = "https://raw.githubusercontent.com/owid/energy-data/master/owid-energy-data.csv"
            df = pd.read_csv(url)

            df.to_csv("downloads/data.csv",  index_col = 0)
            
            self.df = df[df['year']>=1970]

            

    def list_countries(self):
        """
        List all available countries in the data set
        """
        
        unique_countries = set(self.df.country)
        return list(unique_countries)

            
    def compare_consumption(self, countries):
        """
        Takes a list or string, sums all energy consumtion data and displays a 
        dataframe with columns of choosen countries and the type of energy consumtion
        as index.
        """
        if (type(a) != str) & (type(a) != list):
            raise TypeError("Input Variable is not string or list.")
        
        if type(countries) == str:
            countries = [countries]
            
        for i in countries:
            Total_consumption = self.df[(self.df["year"] < 2020) & (self.df["country"] == i)].filter(regex="consumption").sum(axis = 1)
            plt.plot(self.df[(self.df["year"] < 2020) & (self.df["country"] == i)]["year"], Total_consumption, label = i)
        plt.xlabel("Year")
        plt.ylabel("Energy consumption in terawatt-hours")
        plt.legend()
        plt.title("Comparison of Total Energy Consumption of choosen countries")

        plt.show()
        
        
    def compare_gdp(self, countries):
        if type(countries) == str:
            countries = [countries]
        plt.figure(figsize=(20, 10), dpi=80)
        for i in countries:
            plt.plot(self.df[self.df["country"] == i]["year"], self.df[self.df["country"] == i]["gdp"], label = i)  
        plt.xlabel("Year", fontsize = 20)
        plt.ylabel("GDP in trillion", fontsize = 20)
        plt.legend(fontsize = 20)
        plt.title("Comparison of yearly GDP of choosen countries", fontsize = 20)
        plt.show()
        
        
    def gapminder(self, year):
        """
        Takes a year as input and creates a scatterplot with X as GDP, 
        Y as total energy consumption and size as relative population.
        """
        if year != int:
            raise TypeError("Input variable must be an interger.")
        
        x = self.df[(self.df['year'] == year) & (self.df['country'] != 'World')]['gdp']
        y = self.df[(self.df['year'] == year) & (self.df['country'] != 'World')].filter(regex="consumption").sum(axis = 1)
        area = (self.df[(self.df["year"] == year) & (self.df['country'] != 'World')]['population'] / self.df[(energy_data.df['year'] == year) & (self.df['country'] != 'World')]['population'].sum())*100000
        colors = np.random.rand(len(x))

        plt.figure(figsize=(15, 8))
        plt.scatter(x, y, s=area, c=colors, alpha=0.2, linewidths=2)
        plt.xlabel("GDP")
        plt.ylabel("Energy consumption in terawatt-hours")
        plt.title("Comparison of Total Energy Consumption to GDP")

        plt.show()